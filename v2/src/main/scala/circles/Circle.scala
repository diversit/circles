package circles

import java.lang.IllegalAccessException

case class Circle(val name: String, val size: Int) extends Ordered[Circle]{

  def +(other: Circle): (Circle, Circle) = (this, other)

  def -(other: Circle): Circle = this < other match {
    case true => other substract (this, "|")
    case false => this substract (other,  "")
  }

  import scala.math._
  private def substract(other: Circle, absStr: String): Circle = new Circle(absStr+name+"-"+other.name+absStr, abs(size-other.size))

  def compare(that: Circle): Int = this.size - that.size
}

class CircleTuple(val a: Circle, val b: Circle) {

  def +(c: Circle): (Circle, Circle) = {
    def combineInOrder(circle1: Circle, index1: Int,  circle2: Circle, index2: Int) = {
      def combineCircles(c1: Circle, c2: Circle) = new Circle(c1.name+"+"+c2.name, c1.size+c2.size)
      index1 < index2 match {
        case true => (combineCircles(circle1, circle2), index1)
        case false => (combineCircles(circle2, circle1), index2)
      }
    }

    List(a, b, c).zipWithIndex.sorted match {
      case List((c1, i1), (c2, i2), (c3, i3)) => combineInOrder(c1, i1, c2, i2) match {
          case (c: Circle, index: Int) if index < i3 => (c,  c3)
          case (c: Circle, index: Int) if index > i3 => (c3, c)
      }
      case _ => throw new IllegalAccessException
    }
  }

  def -(c: Circle): AnyRef = this > c match {
    case true => (smallest,  biggest - c)
    case false => (smallest - biggest) - c
  }

  def size: Int = a.size + b.size
  def biggest: Circle = if(a > b) a else b
  def smallest: Circle = if(a < b) a else b
  private def > (c: Circle) = this.size > c.size
}