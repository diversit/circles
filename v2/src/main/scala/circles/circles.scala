package object circles {

  implicit def toCircleTuple(tuple: (Circle, Circle)): CircleTuple = new CircleTuple(tuple._1, tuple._2)
}