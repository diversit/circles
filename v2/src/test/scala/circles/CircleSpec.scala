package circles

import org.scalatest.FeatureSpec
import org.scalatest.matchers.MustMatchers

class CircleSpec extends FeatureSpec with MustMatchers {

  feature("Adding circles") {
    scenario("A<B: A + B must equal (A,B)") {
      Circle("A", 8) + Circle("B", 1) must equal (Circle("A", 8), Circle("B", 1))
    }

    scenario("C<A<B: (A,B) + C must equal (A+C, B)") {
      (Circle("A", 8), Circle("B", 20)) + Circle("C", 1) must equal (Circle("A+C", 9), Circle("B", 20))
    }

    scenario("C<B<A: (A,B) + C must equal (A, B+C)") {
      (Circle("A", 20), Circle("B", 8)) + Circle("C", 1) must equal (Circle("A", 20), Circle("B+C", 9))
    }

    scenario("A<B<C: (A,B) + C must equal (A+B, C)") {
      (Circle("A", 1), Circle("B", 8)) + Circle("C", 20) must equal (Circle("A+B", 9), Circle("C", 20))
    }
  }

  feature("Subsctracting circles") {
    scenario("A<B: A - B must equal (|B-A|)") {
      Circle("A", 8) - Circle("B", 20) must equal (Circle("|B-A|", Math.abs(20-8)))
    }

    scenario("C<A<B: (A,B) - C must equal (A, (B-C))") {
      (Circle("A", 8), Circle("B", 20)) - Circle("C", 1) must equal (Circle("A", 8), Circle("B-C", 20-1))
    }

    scenario("A<B<C: (A,B) - C must equal (|C, (|B-A|)|)") {
      (Circle("A", 1), Circle("B", 8)) - Circle("C", 20) must equal (Circle("|C-|B-A||", 20-(8-1)))
    }
  }
}